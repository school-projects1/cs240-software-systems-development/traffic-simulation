#ifndef __VEHICLE_BASE_H__
#define __VEHICLE_BASE_H__

class VehicleBase {
public:
  static int vehicleCount;
  enum VehicleType {
    CAR, SUV, TRUCK
  };
  enum Turn {
    LEFT, RIGHT, STRAIGHT, Null
  };

private:
  int vehicleID;
  VehicleType vehicleType;
  int size;
  Turn turn;


public:
  VehicleBase(VehicleBase::VehicleType type, VehicleBase::Turn turn);

  VehicleBase(const VehicleBase& other);

  ~VehicleBase();

  inline int getVehicleID() { return this->vehicleID; }

  inline VehicleBase::VehicleType getVehicleType() { return this->vehicleType; }

  VehicleBase::Turn getTurn();

  int getSize();


};


#endif

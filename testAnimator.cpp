#include <iostream>
#include <fstream>
#include "Animator.h"
#include "Light.h"
#include "CarGenerator.h"
#include "TrafficSimulation.h"

using namespace std;

int main(int argc, char *argv[]) {

   if(argc < 2)
  {
    cerr << "Need to specify an input file to configure the simulation."<<endl;
    exit(1);
  }

  ifstream inFile;
//    inFile.open("input_example.txt");
  inFile.open(argv[1]);

  if (inFile.fail()) {
    cerr << "Error openning file" << endl;
    exit(1);
  }

  //use a vector to store all parameters
  vector<double> params;
  double param;

  //read until reach the end of file
  while (!inFile.eof()) {
    inFile >> param;
    params.push_back(param);
  }

  //check valid input size
  if (params.size() != 17) {
    cerr << "Wrong input size: " << params.size() - 1 << endl;
    exit(1);
  }

  //check valid yellow light
  if ((int) params.at(3) < 6 || (int) params.at(5) < 6) {
    if ((int) params.at(3) < 6) {
      cerr << "yellow_north_south too small: " << params.at(3) << endl;
      exit(1);
    } else {
      cerr << "yellow_east_west too small: " << params.at(5) << endl;
      exit(1);
    }

  }

  Animator::MAX_VEHICLE_COUNT = 9999;  // vehicles will be displayed with four digits
  //Animator::MAX_VEHICLE_COUNT = 999;  // vehicles will be displayed with three digits
  //Animator::MAX_VEHICLE_COUNT = 99;  // vehicles will be displayed with two digits

  int halfSize = (int) params.at(1);//number of sections before intersection
  Animator anim(halfSize);

  Light nsLight((int) params.at(2), (int) params.at(3), true);//start with ns
  Light ewLight((int) params.at(4), (int) params.at(5), false);

  CarGenerator carGenerator(params.at(6), params.at(7), params.at(8), params.at(9),
                            params.at(10), params.at(11), params.at(12), params.at(13), params.at(14), params.at(15));

  TrafficSimulation sim(carGenerator, (int) params.at(0), ewLight, nsLight, halfSize, anim);
  sim.run();

  return 0;
}

#ifndef __LANE_CPP__
#define __LANE_CPP__

#include <sstream>
#include <stdexcept>
#include <iostream>
#include "Lane.h"

Lane::Lane(int halfSize) {
  interscIndex = halfSize;
  for (int i = 0; i < 2 * halfSize + 2; i++)
    lane.push_back(nullptr);
  //give lane proper size and fill it with nullptr
}

Lane::~Lane() {}

std::vector<shared_ptr<VehicleBase>> Lane::getLane() {
  return lane;
}

int Lane::getLaneSize() {
  return lane.size();
}

int Lane::getInterscIndex() {
  return interscIndex;
}

shared_ptr<VehicleBase> Lane::get(int index) {
  if (index > -1 && index < lane.size())
    return lane[index];

  //if out of bound
  std::stringstream ss;
  ss << "invalid index: " << index << "   lane size: " << lane.size();
  std::string str = ss.str();
  throw std::invalid_argument(str.c_str());
}

//store the input Vehicle* in the input index
void Lane::set(int index, shared_ptr<VehicleBase> vhcPtr) {
  if (index > -1 && index < lane.size())
    lane[index] = vhcPtr;
  else {
    std::stringstream ss;
    ss << "invalid index: " << index << "   lane size: " << lane.size();
    std::string str = ss.str();
    throw std::invalid_argument(str.c_str());
  }
}

//edited so it checks car infront(which will have already moved ahead)
bool Lane::canEnterEntrance() {
  if (lane[1] == nullptr)
    return true;

//    //if there is car in the entrance
//    VehicleBase v = *(lane[1]);
//    int vhcSize = v.getSize();
//
//    for(int i = 1; i < vhcSize; i++) {
//      if (lane[i+1] != nullptr) {
//          if (lane[i]->getVehicleID() != v.getVehicleID())
//            return false;
//      }
//
//      else if(i == vhcSize)
//        return true;
//        //if nullptr is in any of these indices, then false
//      else
//        return false;
//    }
  return false;


}

//if a car will enter intersection in next second, 
//return the turn of that car
VehicleBase::Turn Lane::direcOfTurn() {
  VehicleBase::Turn turn = lane[interscIndex - 1]->getTurn();
  return turn;
}

//return whether a car will be partly inside the first 
//intersection after time seconds(because the first
//intersection matter when its parallel lane has a car
//turning left)
bool Lane::hasCarInIntersc(int time) {
  if (lane[interscIndex - time] == nullptr)
    return false;
  return true;
}

//return time needed for the vehicle to exit the 
//intersection, will be called if there is yellow
//light present, and if the vehicle will enter the
//intersection at next second
int Lane::timeNeedExitIntersc() {
  int vhcSize = lane[interscIndex - 1]->getSize();
  VehicleBase::Turn t = lane[interscIndex - 1]->getTurn();
  if (t == VehicleBase::LEFT) {
    if (vhcSize == 2)
      return 4;
    if (vhcSize == 3)
      return 5;
    return 6;
  }

  if (t == VehicleBase::RIGHT) {
    if (vhcSize == 2)
      return 2;
    if (vhcSize == 3)
      return 3;
    return 4;
  }
  //if straight
  if (vhcSize == 2)
    return 3;
  if (vhcSize == 3)
    return 4;
  return 5;
}

//return whether a car will be entering the intersection
//in next second(a car already partly in the intersection
//does not count, in which case false will be returned) 
bool Lane::hasCarEnterIntersc() {
  if (lane[interscIndex - 1] == nullptr)
    return false;
  int vhcSize = lane[interscIndex - 1]->getSize();
  for (int i = 1; i < vhcSize; i++) {
    if (lane[interscIndex - 1 - i] != lane[interscIndex - 1])
      return false;
  }
  return true;
}

//return whether a vehicle is entering the entrance, in which
//case part of the vehicle is inside the lane but the other
//sections of the vehicle is outside of the lane;
//will return false if the entire sections of the vehicle is 
//inside of the lane
bool Lane::isCarEntering() {
  if (lane[0] == nullptr)
    return false;

  int vhcSize = lane[0]->getSize();
  for (int i = 1; i < vhcSize; i++) {
    if (lane[i] != lane[0])
      return true;
  }
  return false;
}

bool Lane::isCarEnteringIntersc() {
  if (lane[interscIndex - 1] == nullptr)
    return false;

  //if not null pointer
  int vhcSize = lane[interscIndex - 1]->getSize();
  for (int i = 1; i < vhcSize; i++) {
    if (lane[interscIndex - 1 - i] != lane[interscIndex - i])
      return true;
  }
  return false;
}


#endif


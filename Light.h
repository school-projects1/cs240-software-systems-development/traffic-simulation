#ifndef  __Light_H__
#define __Light_H__

#include <string>


class Light {

public:
  enum Color {
    Red, Green, Yellow
  };

protected:
  //double that represent the time a light runs for.
  int redlight;
  int greenlight;
  int yellowlight;
  int timeToChange;
  Color currentLight;
  //One light starts at green, so to begin at green set this to true at the start



public:
  Light();

  Light(int green, int yellow, bool start);

  ~Light();

  void moveOneTick();

  Color getCurrentLight();

  int timeUntilRed();
};

#endif

		
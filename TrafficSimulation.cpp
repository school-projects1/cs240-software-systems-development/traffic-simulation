#ifndef __TrafficSimulation_CPP__
#define __TrafficSimulation_CPP__

#include "TrafficSimulation.h"

using namespace std;

TrafficSimulation::TrafficSimulation() {}


TrafficSimulation::TrafficSimulation(CarGenerator gen, int clockEnd, Light
lightEW, Light lightNS, int halfsize, Animator anim) {
  generator = gen;
  this->clockEnd = clockEnd;
  currClock = 0;
  this->lightNS = lightNS;
  this->lightEW = lightEW;
  size = halfsize;
  this->anim = anim;
}

TrafficSimulation::~TrafficSimulation() {}

//When called this method will run a traffic simulation based on the
//contents of the class.
void TrafficSimulation::run() {

  //initialize
  char dummy;

  Lane northBound(size);
  Lane southBound(size);
  Lane eastBound(size);
  Lane westBound(size);

  //runs until loop has run clockEnd times
  while (currClock < clockEnd) {
    //light update
    if (lightNS.timeUntilRed() == 0)
      isEWRed = false;
    else
      isEWRed = true;


    if (isEWRed)
      processTraffic(northBound, southBound, westBound, eastBound);

    else
      processTraffic(westBound, eastBound, southBound, northBound);

    
    if (northBound.canEnterEntrance() && generator.isCarComingNS()) {
      //get vehicle then send it to the given section
      shared_ptr<VehicleBase> vehicle = generator.getVehicle();
      northBound.set(0, vehicle);
    }


    if (southBound.canEnterEntrance() && generator.isCarComingNS()) {
      //get vehicle then send it to the given section
      shared_ptr<VehicleBase> vehicle = generator.getVehicle();
      southBound.set(0, vehicle);
    }

    if (westBound.canEnterEntrance() && generator.isCarComingEW()) {
      //get vehicle then send it to the given section
      shared_ptr<VehicleBase> vehicle = generator.getVehicle();
      westBound.set(0, vehicle);
    }

    if (eastBound.canEnterEntrance() && generator.isCarComingEW()) {
      //get vehicle then send it to the given section
      shared_ptr<VehicleBase> vehicle = generator.getVehicle();
      eastBound.set(0, vehicle);
    }

    //Sets vehicles according to lawson's animator class to print current status
    anim.setVehiclesNorthbound(northBound.getLane());
    anim.setVehiclesSouthbound(southBound.getLane());
    anim.setVehiclesWestbound(westBound.getLane());
    anim.setVehiclesEastbound(eastBound.getLane());

    anim.draw(currClock);
    std::cin.get(dummy);

    southBound.getLane().assign(size * 2 + 2, nullptr); // reset
    northBound.getLane().assign(size * 2 + 2, nullptr); // reset
    eastBound.getLane().assign(size * 2 + 2, nullptr); // reset
    westBound.getLane().assign(size * 2 + 2, nullptr); // reset

    lightNS.moveOneTick();
    lightEW.moveOneTick();
    currClock++;

  }
}

//Moves cars by one point
void TrafficSimulation::processTraffic(Lane& l1, Lane& l2, Lane& l3, Lane& l4) {

  bool l1Entering = l1.isCarEntering();
  bool l2Entering = l2.isCarEntering();
  bool l3Entering = l3.isCarEntering();
  bool l4Entering = l4.isCarEntering();

  //process area above intersection
  for (int i = size * 2 + 1; i >= size + 2; i--) {

    if (i == size * 2 + 1) {
      l1.set(i, nullptr);
      l2.set(i, nullptr);
      l3.set(i, nullptr);
      l4.set(i, nullptr);
    } else {
      l1.set(i + 1, l1.get(i));
      l1.set(i, nullptr);
      l2.set(i + 1, l2.get(i));
      l2.set(i, nullptr);
      l3.set(i + 1, l3.get(i));
      l3.set(i, nullptr);
      l4.set(i + 1, l4.get(i));
      l4.set(i, nullptr);
    }
  }


  //intersection
  if (l1.get(size + 1) != nullptr && l2.get(size + 1) != nullptr) {
    shared_ptr<VehicleBase> l1v = l1.get(size + 1);
    shared_ptr<VehicleBase> l2v = l2.get(size + 1);
    if (l1v->getTurn() == VehicleBase::LEFT && l2v->getTurn() == VehicleBase::LEFT) {
      //if in both intersections the vehicles are turning left,
      //then both of vehicles are cars
      if (pos1 == VehicleBase::LEFT) {
        l2.set(size, l1.get(size));
        l3.set(size + 1, l1.get(size));
        pos2 = VehicleBase::RIGHT;

        l1.set(size, l2.get(size + 1));
        l4.set(size + 1, l2.get(size + 1));
        pos1 = VehicleBase::RIGHT;
      } else {
        l4.set(size + 2, l1.get(size));
        l3.set(size + 2, l2.get(size));

        l1.set(size + 1, nullptr);
        l3.set(size, nullptr);

        l2.set(size + 1, nullptr);
        l4.set(size, nullptr);
      }
    } else if (l1v->getTurn() == VehicleBase::LEFT && l2v->getTurn() == VehicleBase::STRAIGHT) {
      l2.set(size + 2, l2v);
      l2.set(size + 1, nullptr);
      l4.set(size, nullptr);

      if (l2.get(size) != nullptr) {
        if (pos2 == VehicleBase::RIGHT) {
          l3.set(size + 2, l2.get(size));
        } else {
          //straight or left
          l2.set(size + 1, l2.get(size));
          l4.set(size, l2.get(size));
        }
      }
      l2.set(size, l1v);
      l3.set(size + 1, l1v);
      pos2 = VehicleBase::RIGHT;

      l1.set(size + 1, nullptr);
      l3.set(size, nullptr);

      //move l1 intersection
      if (l1.get(size) != nullptr) {
        if (pos1 == VehicleBase::RIGHT) {
          l4.set(size + 2, l1.get(size));
        } else {
          //straight or left
          l1.set(size + 1, l1.get(size));
          l3.set(size, l1.get(size));
        }
        //set l1 intersc to null
        l1.set(size, nullptr);
        l4.set(size + 1, nullptr);
        pos1 = VehicleBase::Null;
      }
    } else if (l1v->getTurn() == VehicleBase::STRAIGHT && l2v->getTurn() == VehicleBase::LEFT) {
      l1.set(size + 2, l1v);
      l1.set(size + 1, nullptr);
      l3.set(size, nullptr);

      if (l1.get(size) != nullptr) {
        if (pos1 == VehicleBase::RIGHT) {
          l4.set(size + 2, l1.get(size));
        } else {
          //straight or left
          l1.set(size + 1, l1.get(size));
          l3.set(size, l1.get(size));
        }
      }

      l1.set(size, l2v);
      l4.set(size + 1, l2v);
      pos1 = VehicleBase::RIGHT;

      l2.set(size + 1, nullptr);
      l4.set(size, nullptr);

      //move l2 intersection
      if (l2.get(size) != nullptr) {
        if (pos2 == VehicleBase::RIGHT) {
          l3.set(size + 2, l2.get(size));
        } else {
          //straight or left
          l2.set(size + 1, l2.get(size));
          l4.set(size, l2.get(size));
        }
        //set l2 intersc to null
        l2.set(size, nullptr);
        l3.set(size + 1, nullptr);
        pos2 = VehicleBase::Null;
      }
    } else {
      //when both are turning straight
      l1.set(size + 2, l1.get(size + 1));
      l1.set(size + 1, nullptr);
      l3.set(size, nullptr);

      l2.set(size + 2, l2.get(size + 1));
      l2.set(size + 1, nullptr);
      l4.set(size, nullptr);

      //deal with l1 and l2 intersc
      if (l1.get(size) != nullptr) {
        if (pos1 == VehicleBase::RIGHT) {
          l4.set(size + 2, l1.get(size));
        } else {
          //if left/straight
          l1.set(size + 1, l1.get(size));
          l3.set(size, l1.get(size));
        }
        l4.set(size + 1, nullptr);
        l1.set(size, nullptr);
        pos1 = VehicleBase::Null;
      }

      if (l2.get(size) != nullptr) {
        if (pos2 == VehicleBase::RIGHT) {
          l3.set(size + 2, l2.get(size));
        } else {
          //if left/straight
          l2.set(size + 1, l2.get(size));
          l4.set(size, l2.get(size));
        }
        l3.set(size + 1, nullptr);
        l2.set(size, nullptr);
        pos2 = VehicleBase::Null;
      }

    }
  } else if (l1.get(size + 1) != nullptr) {
    //l2.get(size + 1) == nullptr
    if (l2.get(size) != nullptr) {
      if (pos2 == VehicleBase::RIGHT) {
        l3.set(size + 2, l2.get(size));
      } else {
        //straight or left
        l2.set(size + 1, l2.get(size));
        l4.set(size, l2.get(size));
      }
    }

    l2.set(size, nullptr);
    l3.set(size + 1, nullptr);
    pos2 = VehicleBase::Null;

    if (l1.get(size + 1)->getTurn() == VehicleBase::LEFT) {
      l2.set(size, l1.get(size + 1));
      l3.set(size + 1, l1.get(size + 1));
      pos2 = VehicleBase::RIGHT;
    } else {
      //straight
      l1.set(size + 2, l1.get(size + 1));
    }
    l1.set(size + 1, nullptr);
    l3.set(size, nullptr);

    //move l1 intersection
    if (l1.get(size) != nullptr) {
      if (pos1 == VehicleBase::RIGHT) {
        l4.set(size + 2, l1.get(size));
      } else {
        //straight or left
        l1.set(size + 1, l1.get(size));
        l3.set(size, l1.get(size));
      }
      //set l1 intersc to null
      l1.set(size, nullptr);
      l4.set(size + 1, nullptr);
      pos1 = VehicleBase::Null;
    }
  } else if (l2.get(size + 1) != nullptr) {
    //l1.get(size + 1) == nullptr
    if (l1.get(size) != nullptr) {
      if (pos1 == VehicleBase::RIGHT) {
        l4.set(size + 2, l1.get(size));
      } else {
        //straight or left
        l1.set(size + 1, l1.get(size));
        l3.set(size, l1.get(size));
      }
    }

    l1.set(size, nullptr);
    l4.set(size + 1, nullptr);
    pos1 = VehicleBase::Null;

    if (l2.get(size + 1)->getTurn() == VehicleBase::LEFT) {
      l1.set(size, l2.get(size + 1));
      l4.set(size + 1, l2.get(size + 1));
      pos1 = VehicleBase::RIGHT;
    } else {
      //straight
      l2.set(size + 2, l2.get(size + 1));
    }
    l2.set(size + 1, nullptr);
    l4.set(size, nullptr);

    //move l2 intersection
    if (l2.get(size) != nullptr) {
      if (pos2 == VehicleBase::RIGHT) {
        l3.set(size + 2, l2.get(size));
      } else {
        //straight or left
        l2.set(size + 1, l2.get(size));
        l4.set(size, l2.get(size));
      }
      //set l2 intersc to null
      l2.set(size, nullptr);
      l3.set(size + 1, nullptr);
      pos2 = VehicleBase::Null;
    }
  } else {
    //if both nullptr

    //move l1 intersection
    if (l1.get(size) != nullptr) {
      if (pos1 == VehicleBase::RIGHT) {
        l4.set(size + 2, l1.get(size));
      } else {
        //straight or left
        l1.set(size + 1, l1.get(size));
        l3.set(size, l1.get(size));
      }
      //set l1 intersc to null
      l1.set(size, nullptr);
      l4.set(size + 1, nullptr);
      pos1 = VehicleBase::Null;
    }

    //move l2 intersection
    if (l2.get(size) != nullptr) {
      if (pos2 == VehicleBase::RIGHT) {
        l3.set(size + 2, l2.get(size));
      } else {
        //straight or left
        l2.set(size + 1, l2.get(size));
        l4.set(size, l2.get(size));
      }
      //set l2 intersc to null
      l2.set(size, nullptr);
      l3.set(size + 1, nullptr);
      pos2 = VehicleBase::Null;
    }
  }

  bool l1proceed;
  bool l2proceed;


  bool checkOtherLane;
  std::mt19937 rng(8675308);
  std::uniform_real_distribution<double> rand_double(0.0, 1.0);
  double rnd = rand_double(rng);
  if (rnd < 0.5)
    checkOtherLane = true;
  else
    checkOtherLane = false;

  if (l1.hasCarEnterIntersc())
    l1proceed = canProceed(l1, l2, checkOtherLane);

  if (l2.hasCarEnterIntersc())
    l2proceed = canProceed(l2, l1, !checkOtherLane);

  if (l1.isCarEnteringIntersc() || !l1.hasCarEnterIntersc() || l1proceed) {
    if (l1.get(size - 1) != nullptr) {
      l1.set(size, l1.get(size - 1));
      pos1 = l1.get(size)->getTurn();
    }

    for (int i = size - 1; i > 0; i--) {
      l1.set(i, l1.get(i - 1));
    }
    if (!l1Entering)
      l1.set(0, nullptr);
    else
      l1.set(0, l1.get(1));


  }

  if (l2.isCarEnteringIntersc() || !l2.hasCarEnterIntersc() || l2proceed) {
    if (l2.get(size - 1) != nullptr) {
      l2.set(size, l2.get(size - 1));
      pos2 = l2.get(size)->getTurn();
    }

    for (int i = size - 1; i > 0; i--)
      l2.set(i, l2.get(i - 1));

    if (!l2Entering)
      l2.set(0, nullptr);
    else
      l2.set(0, l2.get(1));
  }

  // following code handles cars in l3/l4 moving before the intersection.
  for (int i = size - 1; i > 0; i--) {
    if (l3.get(i) == nullptr) {
      l3.set(i, l3.get(i - 1));
      l3.set(i - 1, nullptr);
    }
  }

  for (int i = size - 1; i > 0; i--) {
    if (l4.get(i) == nullptr) {
      l4.set(i, l4.get(i - 1));
      l4.set(i - 1, nullptr);
    }
  }

  bool trafficJam = false;
  if (!l3Entering) {
    if (l3.get(1) != nullptr) {
      int s = l3.get(1)->getSize();
      for (int i = 1; i < s; i++) {
        if (l3.get(1 + i) != l3.get(1))
          trafficJam = true;
      }
    }
    if (!trafficJam)
      l3.set(0, nullptr);
  } else
    l3.set(0, l3.get(1));

  trafficJam = false;
  if (!l4Entering) {
    if (l4.get(1) != nullptr) {
      int s = l4.get(1)->getSize();
      for (int i = 1; i < s; i++) {
        if (l4.get(1 + i) != l4.get(1))
          trafficJam = true;
      }
    }
    if (!trafficJam)
      l4.set(0, nullptr);
  } else
    l4.set(0, l4.get(1));
}

//Method determines if a given car can move at the intersection
bool TrafficSimulation::canProceed(Lane& l1, Lane& l2, bool checkl2) {
  VehicleBase vehicle = *l1.get(size - 1);

  if (isEWRed) {
    if (l1.get(size) != nullptr) {
      return false;
    }
    if (lightNS.getCurrentLight() == Light::Yellow) {
      if (lightNS.timeUntilRed() > l1.timeNeedExitIntersc()) {
        if (vehicle.getTurn() == VehicleBase::STRAIGHT ||
            vehicle.getTurn() == VehicleBase::RIGHT)
          return true;
        else { //left
          for (int i = 3; i < l1.timeNeedExitIntersc() + 1; i++) {
            if (l2.hasCarInIntersc(i)) {
              if (checkl2 == true) {
                //check whether the vehicle in intersection of l2 is turing left
                if (l2.get(size - i)->getTurn() == VehicleBase::LEFT) {
                  return true;
                } else if (l2.get(size - 1) != nullptr) {
                  if (l2.get(size - 1)->getSize() == 2) {
                    if (l2.get(size - 1)->getTurn() == VehicleBase::LEFT) {
                      if (l2.get(size - 1) == l2.get(size - 2))
                        return true;
                    }
                  }
                }
                return false;
              } else
                return false;
            }
          }
          if (checkl2 != true) {
            if (l2.get(size + 1) != nullptr) {
              if (l2.get(size + 1)->getTurn() == VehicleBase::LEFT)
                return false;
            }
            if (l2.get(size) != nullptr) {
              if (l2.get(size)->getTurn() == VehicleBase::LEFT)
                return false;
            }
            if (l2.get(size - 1) != nullptr) {
              if (l2.get(size - 1)->getTurn() == VehicleBase::LEFT)
                return false;
            }
          }

          return true;//if no car will be in that section during that period of time
        }
      } else
        return false;
    } else if (lightNS.getCurrentLight() == Light::Green) {
      if (vehicle.getTurn() == VehicleBase::LEFT) {
        for (int i = 3; i < l1.timeNeedExitIntersc() + 1; i++) {
          if (l2.hasCarInIntersc(i)) {
            if (checkl2 == true) {
              if (l2.get(size - i)->getTurn() == VehicleBase::LEFT) {
                return true;
              } else if (l2.get(size - 1) != nullptr) {
                if (l2.get(size - 1)->getSize() == 2) {
                  if (l2.get(size - 1)->getTurn() == VehicleBase::LEFT) {
                    if (l2.get(size - 1) == l2.get(size - 2))
                      return true;
                  }
                }
              }
              return false;
            } else
              return false;

          }
        }
        if (checkl2 != true) {
          if (l2.get(size + 1) != nullptr) {
            if (l2.get(size + 1)->getTurn() == VehicleBase::LEFT)
              return false;
          }
          if (l2.get(size) != nullptr) {
            if (l2.get(size)->getTurn() == VehicleBase::LEFT)
              return false;
          }
          if (l2.get(size - 1) != nullptr) {
            if (l2.get(size - 1)->getTurn() == VehicleBase::LEFT)
              return false;
          }
        }

        return true;
      }
      return true;
    }
  } else {
    if (l1.get(size) != nullptr) {
      return false;
    }
    if (lightEW.getCurrentLight() == Light::Yellow) {
      if (lightEW.timeUntilRed() > l1.timeNeedExitIntersc()) {
        if (vehicle.getTurn() == VehicleBase::STRAIGHT ||
            vehicle.getTurn() == VehicleBase::RIGHT)
          return true;
        else { //left
          for (int i = 3; i < l1.timeNeedExitIntersc() + 1; i++) {
            if (l2.hasCarInIntersc(i)) {
              if (checkl2 == true) {
                if (l2.get(size - i)->getTurn() == VehicleBase::LEFT) {
                  return true;
                } else if (l2.get(size - 1) != nullptr) {
                  if (l2.get(size - 1)->getSize() == 2) {
                    if (l2.get(size - 1)->getTurn() == VehicleBase::LEFT) {
                      if (l2.get(size - 1) == l2.get(size - 2))
                        return true;
                    }
                  }
                }
                return false;
              } else
                return false;
            }
          }
          if (checkl2 != true) {
            if (l2.get(size + 1) != nullptr) {
              if (l2.get(size + 1)->getTurn() == VehicleBase::LEFT)
                return false;
            }
            if (l2.get(size) != nullptr) {
              if (l2.get(size)->getTurn() == VehicleBase::LEFT)
                return false;
            }
            if (l2.get(size - 1) != nullptr) {
              if (l2.get(size - 1)->getTurn() == VehicleBase::LEFT)
                return false;
            }
          }


          return true;//if no car will be in that section during that period of time
        }
      } else
        return false;
    } else if (lightEW.getCurrentLight() == Light::Green) {
      if (vehicle.getTurn() == VehicleBase::LEFT) {
        for (int i = 3; i < l1.timeNeedExitIntersc() + 1; i++) {
          if (l2.hasCarInIntersc(i)) {
            if (checkl2 == true) {
              if (l2.get(size - i)->getTurn() == VehicleBase::LEFT) {
                return true;
              } else if (l2.get(size - 1) != nullptr) {
                if (l2.get(size - 1)->getSize() == 2) {
                  if (l2.get(size - 1)->getTurn() == VehicleBase::LEFT) {
                    if (l2.get(size - 1) == l2.get(size - 2))
                      return true;
                  }
                }
              }
              return false;
            } else
              return false;
          }
        }
        if (checkl2 != true) {
          if (l2.get(size + 1) != nullptr) {
            if (l2.get(size + 1)->getTurn() == VehicleBase::LEFT)
              return false;
          }
          if (l2.get(size) != nullptr) {
            if (l2.get(size)->getTurn() == VehicleBase::LEFT)
              return false;
          }
          if (l2.get(size - 1) != nullptr) {
            if (l2.get(size - 1)->getTurn() == VehicleBase::LEFT)
              return false;
          }
        }

        return true;
      }
      return true;
    }
  }
  return true;
}

#endif

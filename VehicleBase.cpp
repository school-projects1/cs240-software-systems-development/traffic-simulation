#ifndef __VEHICLE_BASE_CPP__
#define __VEHICLE_BASE_CPP__

#include "VehicleBase.h"

int VehicleBase::vehicleCount = 0;

//Enhanced Lawson's vehicle class to account for turns/car type/size
VehicleBase::VehicleBase(VehicleBase::VehicleType type, VehicleBase::Turn turn) {
  vehicleCount++;
  vehicleID = vehicleCount;
  vehicleType = type;
  this->turn = turn;
  if (type == VehicleBase::CAR)
    size = 2;
  else if (type == VehicleBase::SUV)
    size = 3;
  else
    size = 4;
}


VehicleBase::VehicleBase(const VehicleBase& other) {
  vehicleID = other.vehicleID;
  vehicleType = other.vehicleType;
  turn = other.turn;
  size = other.size;
}

VehicleBase::~VehicleBase() {}

VehicleBase::Turn VehicleBase::getTurn() {
  return turn;
}

int VehicleBase::getSize() {
  return size;
}

#endif

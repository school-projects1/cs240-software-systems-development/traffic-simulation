#ifndef __ANIMATOR_H__
#define __ANIMATOR_H__

#include <string>
#include <vector>
#include <memory>
#include "VehicleBase.h"
using namespace std;
//==========================================================================
//* class Animator
//* Author: Barry Lawson
//* Date: 22 Apr 2018
//* This class is intended to provide a text-based graphical interface
//* for animating the traffic intersection simulation from CMSC 240.
//* The idea for this class was heavily influenced by code written for
//* a similar project in F2017 by the following students:
//*       Tracy Nguyen, Hannah Small, Toan Nguyen
//*
//* This code requires that the VehicleBase class be a super class of
//* any vehicle class(es) in your implementation.
//*
//* Usage:
//*   - construct an instance of Animator, passing the number of sections
//*     prior to the intersection (e.g., 8 will result in a lane of
//*     (8*2) + 2 = 18 sections)
//*   - construct four std::vector<shared_ptr<VehicleBase>>, one for each direction
//*     of westbound, easbound, southbound, and northbound
//*   - assign a _pointer_ to each VehicleBase vehicle in your simulation
//*     into the entry in the vector corresponding to the section(s)
//*     occupied by that vehicle
//*   - call each of setVehiclesNorthbound, setVehiclesSouthbound,
//*     setVehiclesEastbound, and setVehiclesWestbound, passing the
//*     corresponding std::vector<shared_ptr<VehicleBase>>
//*   - call draw(), passing in the value of the simulation time clock
//*   - then repeat:
//*        - update each std::vector<shared_ptr<VehicleBase>> appropriately
//*        - call each of setVehiclesNorthbound, setVehiclesSouthbound,
//*          setVehiclesEastbound, and setVehiclesWestbound, passing the
//*          corresponding std::vector<shared_ptr<VehicleBase>>
//*        - call draw(), passing in the value of the simulation time clock
//==========================================================================

class Animator
{
private:
  static int         DIGITS_TO_DRAW;
  static std::string SECTION_BOUNDARY_EW;
  static std::string EMPTY_SECTION;

  static const std::string SECTION_BOUNDARY_NS;
  static const std::string ERROR_MSG;

  static const std::string COLOR_RED;
  static const std::string COLOR_GREEN;
  static const std::string COLOR_BLUE;
  static const std::string COLOR_BLACK;

  std::vector<bool> vehiclesAreSet;  // 0:north 1:west 2:south 3:east
  int numSectionsBefore;

  std::string getVehicleColor(shared_ptr<VehicleBase> vptr);

  void drawNorthPortion(int time);
  void drawEastbound();
  void drawEastWestBoundary();
  void drawWestbound();
  void drawSouthPortion();

  std::vector<shared_ptr<VehicleBase>> eastToWest;
  std::vector<shared_ptr<VehicleBase>> westToEast;
  std::vector<shared_ptr<VehicleBase>> northToSouth;
  std::vector<shared_ptr<VehicleBase>> southToNorth;

public:
  static int MAX_VEHICLE_COUNT;

  Animator();
  Animator(int numSectionsBeforeIntersection);
  ~Animator();

  inline void setVehiclesNorthbound(std::vector<shared_ptr<VehicleBase>> vehicles)
  { southToNorth = vehicles;  vehiclesAreSet[0] = true; }
  inline void setVehiclesWestbound(std::vector<shared_ptr<VehicleBase>> vehicles)
  { eastToWest   = vehicles;  vehiclesAreSet[1] = true; }
  inline void setVehiclesSouthbound(std::vector<shared_ptr<VehicleBase>> vehicles)
  { northToSouth = vehicles;  vehiclesAreSet[2] = true; }
  inline void setVehiclesEastbound(std::vector<shared_ptr<VehicleBase>> vehicles)
  { westToEast   = vehicles;  vehiclesAreSet[3] = true; }


  void draw(int time);
};

#endif

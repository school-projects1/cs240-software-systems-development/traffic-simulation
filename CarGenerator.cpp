#ifndef  __CarGenerator_CPP__
#define __CarGenerator_CPP__

#include "CarGenerator.h"
#include "VehicleBase.h"
#include <iostream>


//default constructor.
CarGenerator::CarGenerator() {}

CarGenerator::CarGenerator(double ns, double ew, double car,
                           double suv, double rc, double lc, double rs, double ls, double rt, double lt) {
  this->ns = ns;
  this->ew = ew;
  this->car = car;
  this->suv = suv;
  this->rc = rc;
  this->rs = rs;
  this->rt = rt;
  this->lc = lc;
  this->ls = ls;
  this->lt = lt;
  std::mt19937 rng(3738294);
  std::uniform_real_distribution<double> rand_double(0.0, 1.0);
  this->seed = rng;
  this->numGenerator = rand_double;

}

//destructor
CarGenerator::~CarGenerator() {}

//returns true or false based on probability of a car coming in NS roads
bool CarGenerator::isCarComingNS() {

  std::cout << ns << " " << numGenerator(seed) << std::endl;

  if (numGenerator(seed) <= ns) {
    return true;
  } else {
    return false;
  }


}

//returns true based on probability of a car coming in EW roads
bool CarGenerator::isCarComingEW() {


  double result = numGenerator(seed);

  //if the result is less than or equal to ew true, else false
  if (result <= ew) {
    return true;
  } else {
    return false;
  }

}

//for testing purposes it retuns an int (will eventualy return type Vehicle)
shared_ptr<VehicleBase> CarGenerator::getVehicle() {

  double result1 = numGenerator(seed);


  //First get the type of vehicle using result1.
  if (result1 <= car) {
    //now that we know it is a car we must get the type of turn
    double result2 = numGenerator(seed);
    if (result2 <= rc) {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::CAR, VehicleBase::RIGHT));
      return tmp;
    } else if (result2 <= rc + lc) {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::CAR, VehicleBase::LEFT));
      return tmp;
    } else {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::CAR, VehicleBase::STRAIGHT));
      return tmp;
    }
  } else if (result1 <= (car + suv)) {
    //now that we know it is an suv we must get the type of turn
    double result2 = numGenerator(seed);
    if (result2 <= rs) {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::SUV, VehicleBase::RIGHT));
      return tmp;
    } else if (result2 <= rs + ls) {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::SUV, VehicleBase::LEFT));
      return tmp;
    } else {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::SUV, VehicleBase::STRAIGHT));
      return tmp;
    }
  } else {
    //when this block of code runs it is a truck
    double result2 = numGenerator(seed);
    if (result2 <= rt) {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::TRUCK, VehicleBase::RIGHT));
      return tmp;
    } else if (result2 <= rt + lt) {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::TRUCK, VehicleBase::LEFT));
      return tmp;
    } else {
      shared_ptr<VehicleBase> tmp(new VehicleBase(VehicleBase::TRUCK, VehicleBase::STRAIGHT));
      return tmp;
    }
  }

}

#endif

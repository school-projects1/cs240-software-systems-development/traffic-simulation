#ifndef __TrafficSimulation_H__
#define __TrafficSimulation_H__

#include <iostream>
#include "CarGenerator.h"
#include "Light.h"
#include "Lane.h"
#include "Animator.h"
#include "VehicleBase.h"


using namespace std;

class TrafficSimulation 
{
	private:
		CarGenerator generator;
		int currClock;
		int clockEnd;
		Light lightNS;
		Light lightEW;
		//stores half size of lanes, will be used to make lane objs
		int size;
		Animator anim;
    enum Directions {N, S, E, W};
    bool isEWRed;//decide which lane to process

		VehicleBase::Turn pos1 = VehicleBase::Turn::Null;
		VehicleBase::Turn pos2 = VehicleBase::Turn::Null;

	bool canProceed(Lane& l1, Lane& l2, bool l2check);
	void processTraffic(Lane& l1, Lane& l2, Lane& l3, Lane& l4);



public:
	TrafficSimulation();

	TrafficSimulation(CarGenerator gen, int clockEnd, Light
		lightEW, Light lightNS, int halfsize, Animator anim);

	~TrafficSimulation();

	void run();
	void moveOneSpace();
};
#endif

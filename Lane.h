#ifndef __LANE_H__
#define __LANE_H__

#include "VehicleBase.h"
#include <vector>
#include <memory>

using namespace std;

class Lane {
private:
  vector<shared_ptr<VehicleBase>> lane;
  int interscIndex;

public:
  Lane(int halfSize);

  ~Lane();

  vector<shared_ptr<VehicleBase>> getLane();

  int getLaneSize();

  int getInterscIndex();

  shared_ptr<VehicleBase> get(int index);

  void set(int index, shared_ptr<VehicleBase> vhcPtr);

  bool canEnterEntrance();

  VehicleBase::Turn direcOfTurn();    //if a car will enter intersection in next
  //second, what is the turn of that car
  bool hasCarInIntersc(int time);    //whether a car will be inside the front
  //intersection after time seconds
  int timeNeedExitIntersc();

  bool hasCarEnterIntersc();    //whether a car will be entering the intersection
  //in next second(a car already partly in the
  //intersection does not count)
  bool isCarEntering();      //checks whether a car is entering the entrance

  bool isCarEnteringIntersc();


};

#endif


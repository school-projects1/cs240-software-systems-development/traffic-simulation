#ifndef  __CarGenerator_H__
#define __CarGenerator_H__

#include <string>
#include <random>
#include <memory>

#include "VehicleBase.h"

using namespace std;

class CarGenerator {

private:
  //adds straight, truck vars to make programming simpler
  double ns;
  double ew;
  double car;
  double suv;
  double rc;
  double rs;
  double rt;
  double lc;
  double ls;
  double lt;
  std::mt19937 seed;
  std::uniform_real_distribution<double> numGenerator;


public:
  CarGenerator();

  CarGenerator(double ns, double ew, double car, double suv, double
  rc, double lc, double rs, double ls, double rt, double lt); //currently ignores left turns

  ~CarGenerator();

  bool isCarComingNS();

  bool isCarComingEW();

  //for testing purposes it retuns an string (will eventualy return type Vehicle)
  shared_ptr<VehicleBase> getVehicle();


};

#endif

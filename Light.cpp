#ifndef  __Light_CPP__
#define __Light_CPP__

#include "Light.h"


Light::Light() {}

//Constructor for the light class.
Light::Light(int green, int yellow, bool start) {
  greenlight = green;
  yellowlight = yellow;
  redlight = green + yellow;
  //The boolean in the constructor specifies if it should start off
  //true or false.
  if (start) {
    currentLight = Light::Green;
    timeToChange = greenlight;
  } else {
    currentLight = Light::Red;
    timeToChange = redlight;
  }

}

Light::~Light() {}


// moves the timeToChange down by 1, if it reaches 0, changes the light.
void Light::moveOneTick() {
  timeToChange = timeToChange - 1;
  //When timeToChange is 0 the light is ready to change colors
  if (timeToChange == 0) {
    //
    if (currentLight == Light::Green) {
      //green to yellow
      currentLight = Light::Yellow;
      timeToChange = yellowlight;

    } else if (currentLight == Light::Yellow) {
      //yellow to red
      currentLight = Light::Red;
      timeToChange = redlight;
    } else {
      //red to green
      currentLight = Light::Green;
      timeToChange = greenlight;
    }
  }
}

// Returns the current light.
Light::Color Light::getCurrentLight() {
  return currentLight;
}

//Returns the time until a given light will be red (0 if red)
int Light::timeUntilRed() {
  if (currentLight == Light::Red) {
    return 0;
  } else if (currentLight == Light::Yellow) {
    return timeToChange;
  } else {
    return (timeToChange + yellowlight);
  }

}

#endif